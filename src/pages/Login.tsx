import React, { useState } from "react";
import { Box, Grid, TextField, Typography } from "@mui/material";
import ButtonLoader from "../Components/ButtonLoader";
import { useMutation } from "react-query";
import { LOGIN } from "react-Query/Keys";
import { _AuthService } from "services/auth/auth.service";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const [form, setForm] = useState<{ email: string; password: string }>({
    email: "",
    password: "",
  });
  const navigate = useNavigate();
  const { mutate: login, isLoading } = useMutation(
    [LOGIN],
    (formData: FormData) => _AuthService.login(formData).then((res) => res),
    {
      onSuccess: () => {
        navigate("/dashboard/tickets");
      },
    }
  );

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setForm({ ...form, [name]: value });
  };
  const handleSubmit = () => {
    const formData = new FormData();
    formData.append("email", form?.email);
    formData.append("password", form?.password);
    login(formData);
  };
  return (
    <Box
      sx={{
        minHeight: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#151C24",
      }}
    >
      <Box sx={{ minWidth: "350px", width: "30%", padding: "40px" }}>
        <Grid
          container
          direction="column"
          justifyContent="center"
          alignItems="center"
        >
          <Box sx={{ width: "120px" }}></Box>
          <Typography variant="h5" sx={{ color: "#00568B", mt: "10px" }}>
            Hi, Welcome Back
          </Typography>
          <Typography variant="h6" sx={{ color: "#FFFFFF", mt: "10px" }}>
            Enter your credentials to continue
          </Typography>
        </Grid>
        <Box sx={{ width: "100%" }}>
          <Box sx={{ width: "100%", mt: "20px" }}>
            <Box sx={{ margin: "0 0 8px 5px" }}>
              <Typography sx={{ color: "#FFFFFF" }}>Name</Typography>
            </Box>
            <TextField
              sx={{
                width: "100%",
                color: "#FFFFFF",
                "& .MuiInputBase-root": {
                  color: "#FFFFFF",
                },
              }}
              type="email"
              name="email"
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                handleChange(e)
              }
              placeholder="Enter your email"
            />
          </Box>
          <Box sx={{ width: "100%", mt: "20px" }}>
            <Box sx={{ margin: "0 0 8px 5px" }}>
              <Typography
                sx={{
                  color: "#FFFFFF",
                }}
              >
                Password
              </Typography>
            </Box>
            <TextField
              sx={{
                width: "100%",
                color: "#FFFFFF",
                "& .MuiInputBase-root": {
                  color: "#FFFFFF",
                },
              }}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                handleChange(e)
              }
              type="password"
              name="password"
              placeholder="Enter your password"
            />
          </Box>
          <Box sx={{ marginTop: "30px", width: "100%" }}>
            <ButtonLoader
              onClick={handleSubmit}
              disableOnLoading
              loading={isLoading}
              fullWidth
              type="button"
              variant="contained"
            >
              Sign In
            </ButtonLoader>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Login;
