import React, { useEffect } from "react";
import { Box } from "@mui/material";
import AppRouting from "./AppRouting";
import "./App.css";
import { HttpRequestInterceptor } from "interceprors/http-request";

const App = () => {
  useEffect(() => {
    HttpRequestInterceptor();
  }, []);
  return (
    <Box>
      <AppRouting />
    </Box>
  );
};

export default App;
