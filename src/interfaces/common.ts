export interface RootObj<T = any> {
  result: T;
  status: boolean;
  message: string;
  code: number;
}
