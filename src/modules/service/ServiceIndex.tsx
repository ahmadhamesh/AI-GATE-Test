import React from "react";
import {
  Box,
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { CREATE_TICKET, GET_SERVICES } from "react-Query/Keys";
import { useMutation, useQuery } from "react-query";
import { _TicketsService } from "services/tickets/tickets.service";
import { IService } from "services/tickets/tickets";
import { useNavigate } from "react-router-dom";

const TableCells = ["Title", "Price", "Action"];

const ServiceIndex = () => {
  const navigate = useNavigate();
  const { data: serviceData } = useQuery(
    [GET_SERVICES],
    () => _TicketsService.getServices().then((res) => res[0]),
    {
      refetchOnWindowFocus: false,
    }
  );
  const { mutate: createTicket } = useMutation(
    [CREATE_TICKET],
    (form: FormData) => _TicketsService.createTicket(form).then((res) => res),
    {
      onSuccess: () => {
        navigate("/dashboard/tickets");
      },
    }
  );
  const handleCreateTicket = (id: number) => {
    const formData = new FormData();
    formData.append("service_id", `${id}`);
    createTicket(formData);
  };

  return (
    <Box
      sx={{
        minHeight: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        backgroundColor: "#151C24",
        pt: "4rem",
        pb: "4rem",
        gap: "1rem",
      }}
    >
      <Typography variant="h4" sx={{ color: "#FFFFFF" }}>
        All Services
      </Typography>
      <TableContainer
        sx={{ width: "70%", backgroundColor: "#212B35", borderRadius: "10px" }}
      >
        <Table>
          <TableHead>
            <TableRow>
              {TableCells.map((tableCell, i) => (
                <TableCell align="center" key={i} sx={{ color: "#FFFFFF" }}>
                  {tableCell}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {serviceData &&
              serviceData.map((service: IService, i: number) => (
                <TableRow key={i}>
                  <TableCell align="center" sx={{ color: "#FFFFFF" }}>
                    {service?.title}
                  </TableCell>
                  <TableCell align="center" sx={{ color: "#FFFFFF" }}>
                    {service?.price}
                  </TableCell>
                  <TableCell align="center" sx={{ color: "#FFFFFF" }}>
                    <Button onClick={() => handleCreateTicket(service.id)}>
                      Create New Ticket
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default ServiceIndex;
