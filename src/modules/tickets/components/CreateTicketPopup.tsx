import { Box, Button } from "@mui/material";
import React from "react";

const CreateTicketPopup = () => {
  const handleClose = () => {};
  return (
    <Box
      sx={{
        width: "400px",
        height: "200px",
        backgroundColor: "#151C24",
        borderRadius: "14px",
        padding: "1rem",
        display: "flex",
        justifyContent: "center",
        alignItems: "start",
        flexDirection: "column",
      }}
    >
      <Box sx={{ display: "flex", justifyContent: "end", width: "100%" }}>
        <Button
          sx={{ color: "#FFFFFF", width: "30px", height: "30px" }}
          onClick={handleClose}
        >
          X
        </Button>
      </Box>
    </Box>
  );
};

export default CreateTicketPopup;
