import { Box, Button, TextField } from "@mui/material";
import { useQueryParams } from "hooks/useQueryParams";
import React, { useState } from "react";
import { ADD_REPORT } from "react-Query/Keys";
import { useMutation } from "react-query";
import { useParams } from "react-router-dom";
import { _TicketsService } from "services/tickets/tickets.service";

interface Props {
  handleClose: () => void;
}

const AddReportPopup = (props: Props) => {
  const { handleClose } = props;
  const [report, setReport] = useState("");
  const [setQuery, getQuery, deleteQuery] = useQueryParams();
  const { mutate: addReport } = useMutation(
    [ADD_REPORT],
    (form: FormData) => _TicketsService.addReport(form).then((res) => res),
    {
      onSuccess: () => {
        handleClose();
      },
    }
  );
  const params = getQuery();
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setReport(e?.target?.value);
  };
  const handleSubmit = () => {
    const formData = new FormData();
    formData.append("ticket_id", `${params?.id}`);
    formData.append("report", report);
    addReport(formData);
  };

  return (
    <Box
      sx={{
        width: "400px",
        height: "200px",
        backgroundColor: "#151C24",
        borderRadius: "14px",
        padding: "1rem",
        display: "flex",
        justifyContent: "center",
        alignItems: "start",
        flexDirection: "column",
      }}
    >
      <Box sx={{ display: "flex", justifyContent: "end", width: "100%" }}>
        <Button
          sx={{ color: "#FFFFFF", width: "30px", height: "30px" }}
          onClick={handleClose}
        >
          X
        </Button>
      </Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          gap: "10px",
          flexDirection: "column",
          width: "100%",
        }}
      >
        <TextField
          value={report}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => handleChange(e)}
        />
        <Button variant="contained" onClick={handleSubmit}>
          Add Report
        </Button>
      </Box>
    </Box>
  );
};

export default AddReportPopup;
