import { Box, Button, Typography } from "@mui/material";
import React from "react";
import { ITicket } from "services/tickets/tickets";

interface Props {
  ticketData: ITicket;
  handleClose: () => void;
}

const TicketDetailsPopup = (props: Props) => {
  const { ticketData, handleClose } = props;
  return (
    <Box
      sx={{
        width: "400px",
        height: "200px",
        backgroundColor: "#151C24",
        borderRadius: "14px",
        padding: "1rem",
        display: "flex",
        justifyContent: "center",
        alignItems: "start",
        flexDirection: "column",
      }}
    >
      <Box sx={{ display: "flex", justifyContent: "end", width: "100%" }}>
        <Button
          sx={{ color: "#FFFFFF", width: "30px", height: "30px" }}
          onClick={handleClose}
        >
          X
        </Button>
      </Box>
      <Typography sx={{ color: "#FFFFFF" }}>
        Email : {ticketData?.client?.email}
      </Typography>
      <Typography sx={{ color: "#FFFFFF" }}>
        Name : {ticketData?.client?.name}
      </Typography>
      <Typography sx={{ color: "#FFFFFF" }}>
        role : {ticketData?.client?.role_id === 1 ? "Admin" : "User"}
      </Typography>
      <Typography sx={{ color: "#FFFFFF" }}>
        status : {ticketData?.status?.title}
      </Typography>
      <Typography sx={{ color: "#FFFFFF" }}>
        Service : {ticketData?.service?.title}
      </Typography>
      <Typography sx={{ color: "#FFFFFF" }}>
        Price : {ticketData?.service?.price}
      </Typography>
    </Box>
  );
};

export default TicketDetailsPopup;
