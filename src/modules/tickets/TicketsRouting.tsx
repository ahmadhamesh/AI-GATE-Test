import React from "react";
import TicketsComponent from "./TicketsComponent";
import { Route, Routes } from "react-router-dom";
import TicketsIndex from "./pages/TicketsIndex";

const TicketsRouting = () => {
  return (
    <Routes>
      <Route path="/" element={<TicketsComponent />}>
        <Route path="/" element={<TicketsIndex />} />
        {/* <Route path="/create" element={<CategoriesCreate />} /> */}

        <Route path="*" element={<p>not found 404</p>} />
      </Route>
    </Routes>
  );
};

export default TicketsRouting;
