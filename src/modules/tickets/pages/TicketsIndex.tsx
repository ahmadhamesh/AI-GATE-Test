import {
  Box,
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Dialog,
  TextField,
} from "@mui/material";
import { useQueryParams } from "../../../hooks/useQueryParams";
import React, { useState, useEffect } from "react";
import {
  DELETE_TICKET,
  GET_TICKET,
  GET_TICKETS,
  LOGOUT,
} from "react-Query/Keys";
import { useMutation, useQuery } from "react-query";
import { _TicketsService } from "services/tickets/tickets.service";
import TicketDetailsPopup from "../components/TicketDetailsPopup";
import { ITicket, ITicketData } from "services/tickets/tickets";
import { useNavigate } from "react-router-dom";
import { _AuthService } from "services/auth/auth.service";
import AddReportPopup from "../components/AddReportPopup";

const TableCells = [
  "id",
  "Client Name",
  "Email",
  "Services",
  "Price",
  "State",
  "Action",
  "Reports",
];

const TicketsIndex = () => {
  const [modaDetailslOpen, setModalDetailsOpen] = useState<boolean>(false);
  const [modaReportlOpen, setModalReportOpen] = useState<boolean>(false);
  const [ticketsData, setTicketsData] = useState<ITicketData[]>([]);
  const [searchId, setSearchId] = useState<number | null>(null);
  const [setQuery, getQuery, deleteQuery] = useQueryParams();
  const params = getQuery();
  const navigate = useNavigate();
  const { data: allTickets, refetch: refetchTickets } = useQuery(
    [GET_TICKETS],
    () =>
      _TicketsService.getTickets().then((res) => {
        setTicketsData(res?.data);
        return res?.data;
      }),
    {
      refetchOnWindowFocus: false,
    }
  );
  const { data: ticketData, refetch: refetchTicket } = useQuery(
    [GET_TICKET],
    () =>
      _TicketsService.getTicketById(params?.id as number).then((res) => res[0]),
    {
      enabled: false,
    }
  );
  const { mutate: logout } = useMutation(
    [LOGOUT],
    () => _AuthService.logout().then((res) => res),
    {
      onSuccess: () => {
        navigate("/");
      },
    }
  );
  const { mutate: deleteTicket } = useMutation(
    [DELETE_TICKET],
    (id: number) => _TicketsService.deleteTicket(id).then((res) => res),
    {
      onSuccess: () => {
        refetchTickets();
      },
    }
  );
  const handleShowDetails = (id: number) => {
    setQuery({ id: id });
    setModalDetailsOpen(true);
  };
  const handleOpenAddReport = (id: number) => {
    setQuery({ id: id });
    setModalReportOpen(true);
  };
  const handleCloseReport = () => {
    setModalReportOpen(false);
    deleteQuery();
  };
  const handleCloseDetails = () => {
    setModalDetailsOpen(false);
    deleteQuery();
  };
  const handleNavigateToServices = () => {
    navigate("/dashboard/service");
  };

  const handleDeleteTicket = (id: number) => {
    deleteTicket(id);
  };
  const handleSearch = () => {
    if (searchId !== null && allTickets && allTickets.length > 0) {
      const filteredData = allTickets.filter(
        (item: ITicketData) => item.id === searchId
      );
      setTicketsData(filteredData);
    } else {
      setTicketsData(allTickets as ITicketData[]);
    }
  };
  useEffect(() => {
    refetchTicket();
  }, [params?.id]);

  return (
    <Box
      sx={{
        minHeight: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        backgroundColor: "#151C24",
        pt: "4rem",
        pb: "4rem",
        gap: "1rem",
      }}
    >
      <Typography variant="h4" sx={{ color: "#FFFFFF" }}>
        All Tickets
      </Typography>
      <TableContainer
        sx={{ width: "85%", backgroundColor: "#212B35", borderRadius: "10px" }}
      >
        <Table>
          <TableHead>
            <TableRow>
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "start",
                  pt: "10px",
                  pl: "8px",
                  gap: "1rem",
                }}
              >
                <Button
                  variant="contained"
                  onClick={handleNavigateToServices}
                  sx={{ width: "170px", height: "40px" }}
                >
                  go to Services
                </Button>
                <TextField
                  sx={{
                    "& .MuiInputBase-root": {
                      width: "170px",
                      height: "40px",
                      color: "#FFFFFF",
                    },
                  }}
                  onChange={(e) =>
                    setSearchId(
                      e.target.value ? parseInt(e.target.value) : null
                    )
                  }
                  onBlur={handleSearch}
                  placeholder="search by id"
                />
              </Box>
            </TableRow>
            <TableRow>
              {TableCells.map((tableCell, i) => (
                <TableCell
                  align={tableCell === "Action" ? "center" : "inherit"}
                  key={i}
                  sx={{ color: "#FFFFFF" }}
                >
                  {tableCell}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {ticketsData &&
              ticketsData.map((ticket, i) => (
                <TableRow key={i}>
                  <TableCell sx={{ color: "#FFFFFF", width: "10%" }}>
                    {ticket?.id}
                  </TableCell>
                  <TableCell sx={{ color: "#FFFFFF" }}>
                    {ticket?.client?.name}
                  </TableCell>
                  <TableCell sx={{ color: "#FFFFFF" }}>
                    {ticket?.client?.email}
                  </TableCell>
                  <TableCell sx={{ color: "#FFFFFF" }}>
                    {ticket?.service?.title}
                  </TableCell>
                  <TableCell sx={{ color: "#FFFFFF" }}>
                    {ticket?.service?.price}
                  </TableCell>
                  <TableCell sx={{ color: "#FFFFFF" }}>
                    {ticket?.status?.title}
                  </TableCell>
                  <TableCell
                    sx={{ color: "#FFFFFF", display: "flex", gap: "7px" }}
                  >
                    <Button onClick={() => handleShowDetails(ticket.id)}>
                      Details
                    </Button>
                    <Button onClick={() => handleDeleteTicket(ticket.id)}>
                      Delete
                    </Button>
                  </TableCell>
                  <TableCell>
                    <Button onClick={() => handleOpenAddReport(ticket.id)}>
                      Add
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Dialog
        sx={{
          "& .MuiPaper-root": {
            backgroundColor: "#151C24",
            borderRadius: "14px",
          },
        }}
        open={modaDetailslOpen}
        onClose={handleCloseDetails}
      >
        <TicketDetailsPopup
          handleClose={handleCloseDetails}
          ticketData={ticketData as ITicket}
        />
      </Dialog>
      <Dialog
        sx={{
          "& .MuiPaper-root": {
            backgroundColor: "#151C24",
            borderRadius: "14px",
          },
        }}
        open={modaReportlOpen}
        onClose={handleCloseReport}
      >
        <AddReportPopup handleClose={handleCloseReport} />
      </Dialog>
      <Box onClick={() => logout()}>
        <Typography
          sx={{
            color: "blue",
            cursor: "pointer",
            "&:hover": {
              textDecoration: "underline",
            },
          }}
        >
          Logout
        </Typography>
      </Box>
    </Box>
  );
};

export default TicketsIndex;
