import React from "react";
import { Outlet } from "react-router-dom";

const TicketsComponent = () => {
  return (
    <>
      <Outlet />
    </>
  );
};

export default TicketsComponent;
