import React from "react";
import { Route, Routes } from "react-router-dom";
import DashboardComponent from "./DashboardComponent";
import Loader from "Components/Loader";
import TicketsRouting from "modules/tickets/TicketsRouting";
import ServiceIndex from "modules/service/ServiceIndex";

const DashboardRouting = () => {
  return (
    <Routes>
      <Route element={<DashboardComponent />}>
        <Route
          path="/tickets/*"
          element={
            <React.Suspense fallback={<Loader />}>
              <TicketsRouting />
            </React.Suspense>
          }
        />
        <Route path="/service" element={<ServiceIndex />} />
      </Route>
    </Routes>
  );
};

export default DashboardRouting;
