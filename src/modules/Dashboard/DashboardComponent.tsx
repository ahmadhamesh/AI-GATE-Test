import { Box } from "@mui/material";
import React, { useState } from "react";
import { Outlet } from "react-router-dom";

const DashboardComponent = () => {
  return (
    <Box
      sx={{
        // display: "flex",
        backgroundColor: "#151C24",
        minHeight: "100vh",
        minWidth: "100vw",
      }}
    >
      <Box>
        <Box>
          <Outlet />
        </Box>
      </Box>
    </Box>
  );
};

export default DashboardComponent;
