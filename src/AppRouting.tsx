import React from "react";
import { Route, Routes } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "react-query";
import ShouldBeLogged from "./middlewares/ShouldBeLogged";
import ShouldNotBeLogged from "./middlewares/ShouldNotBeLogged";
import { ReactQueryDevtools } from "react-query/devtools";
import Login from "./pages/Login";
import DashboardRouting from "modules/Dashboard/DashboardRouting";
import Loader from "Components/Loader";

const AppRouting = () => {
  const queryClient = new QueryClient();

  return (
    <QueryClientProvider client={queryClient}>
      <Routes>
        <Route
          path="/"
          element={
            <ShouldNotBeLogged>
              <Login />
            </ShouldNotBeLogged>
          }
        />
        <Route
          path="dashboard/*"
          element={
            <ShouldBeLogged>
              <React.Suspense fallback={<Loader />}>
                <DashboardRouting />
                <ReactQueryDevtools initialIsOpen={false} />
              </React.Suspense>
            </ShouldBeLogged>
          }
        />
      </Routes>
    </QueryClientProvider>
  );
};

export default AppRouting;
