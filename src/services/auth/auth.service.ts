import { RootObj } from "../../interfaces/common";
import { _axios } from "../http-config";
import { LoginBodyPost, LoginResponse } from "./auth";

const { REACT_APP_Token_key } = process.env;

class AuthService {
  private static _instance: AuthService;
  private readonly URL = "api";

  public static get Instance() {
    return this._instance || (this._instance = new this());
  }

  login(data: FormData): Promise<LoginResponse> {
    return _axios.post<LoginResponse>(`login`, data).then((res) => {
      this.doLogin(res.data.access_token);
      return res.data;
    });
  }
  logout() {
    return _axios.delete(`logout`).then((res) => {
      this.doLogout();
    });
  }
  doLogin(token: any, user?: any) {
    this.storeTokens(token);
  }
  isLoggedIn() {
    return Boolean(this.getJwtToken());
  }

  doLogout() {
    this.destroyTokens();
  }
  private destroyTokens() {
    localStorage.removeItem(REACT_APP_Token_key ?? "token");
  }

  getJwtToken() {
    return localStorage.getItem("token");
  }

  private storeTokens(token: any) {
    localStorage.setItem("token", token);
  }
}
export const _AuthService = AuthService.Instance;
