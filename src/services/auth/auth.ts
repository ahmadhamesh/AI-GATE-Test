export interface IUser {
  id: number;
  role_id: 1 | 0;
  name: string;
  email: string;
}
export interface LoginBodyPost {
  email: string;
  password: string;
}
export interface IRegisterBody {
  name: string;
  email: string;
  password: string;
  role_id: 1 | 0;
}
export interface LoginResponse {
  user: IUser;
  message: string;
  access_token: string;
}
