export interface ITicketData {
  id: number;
  client: {
    id: number;
    role_id: 1 | 2;
    name: string;
    email: string;
  };
  service: {
    id: number;
    title: string;
    price: number;
  };
  status: {
    id: number;
    title: string;
  };
}
export interface ITickets {
  current_page: number;
  data: ITicketData[];
  first_page_ur: string;
  from: number;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url: number | null;
  to: number;
}

export interface ITicket extends ITicketData {
  reports: { id: number; report: string }[];
}
export interface IService {
  id: number;
  title: string;
  price: number;
}
