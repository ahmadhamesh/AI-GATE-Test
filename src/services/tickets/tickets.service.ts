import { RootObj } from "../../interfaces/common";
import { _axios } from "../http-config";
import { IService, ITicket, ITickets } from "./tickets";

const { REACT_APP_Token_key } = process.env;

class TicketsService {
  private static _instance: TicketsService;
  private readonly URL = "api";

  public static get Instance() {
    return this._instance || (this._instance = new this());
  }

  getTickets(): Promise<ITickets> {
    return _axios.get<ITickets>(`get-tickets`).then((res) => {
      return res.data;
    });
  }
  getTicketById(id: number) {
    return _axios.get<ITicket[]>(`view-ticket/${id}`).then((res) => res.data);
  }
  addReport(form: FormData) {
    return _axios.post(`add-report`, form).then((res) => res?.data);
  }

  createTicket(form: FormData) {
    return _axios.post(`create-ticket`, form).then((res) => res?.data);
  }
  deleteTicket(id: number) {
    return _axios.delete(`delete-ticket/${id}`).then((res) => res?.data);
  }
  getServices() {
    return _axios.get<any>(`view-services`).then((res) => res?.data);
  }
}
export const _TicketsService = TicketsService.Instance;
