import axios from "axios";

const baseUrl = process.env.REACT_APP_BASE_URL;

export const _axios = axios.create({
  baseURL: baseUrl,
});
