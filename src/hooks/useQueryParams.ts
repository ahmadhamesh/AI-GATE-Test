import _ from "lodash";
import { useSearchParams } from "react-router-dom";

export const useQueryParams = (): Function[] => {
  const [searchParams, setSearchParams] = useSearchParams();

  const set = (params: any) => {
    setSearchParams({ qq: JSON.stringify(params) });
  };

  const get = () => {
    try {
      return JSON.parse(Object.fromEntries(searchParams)?.qq);
    } catch (error) {
      return {};
    }
  };

  const deleteAll = () => {
    setSearchParams({});
  };

  return [set, get, deleteAll];
};
