import { Navigate } from "react-router-dom";
import { _AuthService } from "../services/auth/auth.service";
import React from "react";

const ShouldNotBeLogged = ({ children }: { children: JSX.Element }) => {
  if (_AuthService.isLoggedIn()) {
    return <Navigate to="/dashboard/tickets" />;
  }
  console.log(_AuthService.isLoggedIn());

  return children;
};

export default ShouldNotBeLogged;
