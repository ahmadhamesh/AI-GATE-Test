import React from "react";
import { Navigate, useLocation } from "react-router-dom";
import { _AuthService } from "../services/auth/auth.service";

const ShouldBeLogged = ({ children }: { children: JSX.Element }) => {
  let location = useLocation();

  if (!_AuthService.isLoggedIn()) {
    // Redirect them to the /login page, but save the current location they were
    // trying to go to when they were redirected. This allows us to send them
    // along to that page after they login, which is a nicer user experience
    // than dropping them off on the home page.
    return <Navigate to="/" state={location.pathname} replace />;
    // return <Navigate to="/home" />;
  }

  return children;
};

export default ShouldBeLogged;
