// auth
export const LOGIN = "login";
export const LOGOUT = "logout";

//tickets
export const GET_TICKETS = "getTickets";
export const GET_TICKET = "getTicket";
export const CREATE_TICKET = "createTicket";
export const DELETE_TICKET = "deleteTicket";
export const ADD_REPORT = "addReport";

//service
export const GET_SERVICES = "getServices";
